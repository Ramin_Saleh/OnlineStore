﻿using System.ComponentModel.DataAnnotations;

namespace OnlineStoreMVC.Models
{
    public class GoodRegisterViewModels
    {
        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "نام کالا")]
        public string Name { get; set; }

        [Display(Name = "شرح کالا")]
        public string Description { get; set; }

        [Display(Name = "گروه کالا")]
        public long IdGroup { get; set; }

        [Display(Name = "نویسنده")]
        public string Author { get; set; }

        [Display(Name = "مترجم")]
        public string Translator { get; set; }

        [Display(Name = "قیمت")]
        public long Price { get; set; }

        [Display(Name = "انتشارات")]
        public string Publisher { get; set; }

        [Display(Name = "ویرایش")]
        public int Edition { get; set; }

        [Display(Name = "کد کالا")]
        public string Code { get; set; }
    }

    public class GoodEditViewModels
    {
        public GoodEditViewModels() { }
        public GoodEditViewModels(Good good)
        {
            Id = good.Id;
            Name = good.Name;
            IdGroup = good.IdGroup;
            Description = good.Description;
            Author = good.Author;
            Translator = good.Translator;
            Price = good.Price ?? 0;
            Publisher = good.Publisher;
            Edition = good.Edition ?? 0;
            Code = good.Code;
        }
        [Key]
        public long Id { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "نام کالا")]
        public string Name { get; set; }

        [Display(Name = "شرح کالا")]
        public string Description { get; set; }

        [Display(Name = "گروه کالا")]
        public long IdGroup { get; set; }

        [Display(Name = "نویسنده")]
        public string Author { get; set; }

        [Display(Name = "مترجم")]
        public string Translator { get; set; }

        [Display(Name = "قیمت")]
        public long Price { get; set; }

        [Display(Name = "انتشارات")]
        public string Publisher { get; set; }

        [Display(Name = "ویرایش")]
        public int Edition { get; set; }

        [Display(Name = "کد کالا")]
        public string Code { get; set; }
    }

    public class GoodViewModel
    {
        public GoodViewModel() { }
        public GoodViewModel(Good good)
        {
            Id = good.Id;
            Name = good.Name;
            IdGroup = good.IdGroup;
            Description = good.Description;
            Author = good.Author;
            Translator = good.Translator;
            Price = good.Price ?? 0;
            Publisher = good.Publisher;
            Edition = good.Edition ?? 0;
            Code = good.Code;
        }
        [Key]
        public long Id { get; set; }

        [Display(Name = "نام کالا")]
        public string Name { get; set; }

        [Display(Name = "شرح کالا")]
        public string Description { get; set; }

        [Display(Name = "گروه کالا")]
        public long IdGroup { get; set; }

        [Display(Name = "نویسنده")]
        public string Author { get; set; }

        [Display(Name = "مترجم")]
        public string Translator { get; set; }

        [Display(Name = "قیمت")]
        public long Price { get; set; }

        [Display(Name = "انتشارات")]
        public string Publisher { get; set; }

        [Display(Name = "ویرایش")]
        public int Edition { get; set; }

        [Display(Name = "کد کالا")]
        public string Code { get; set; }
    }
}