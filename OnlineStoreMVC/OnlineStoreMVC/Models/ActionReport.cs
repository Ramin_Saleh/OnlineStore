﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineStoreMVC.Models
{
    public class ActionReport
    {
        public string Message;
        public bool HasError;
        public ActionReport()
        {
            Message = "عملیات با موفقیت انجام شد";
            HasError = false;
        }
        public ActionReport(string msg)
        {
            Message = msg;
            HasError = true;
        }
    }
}
