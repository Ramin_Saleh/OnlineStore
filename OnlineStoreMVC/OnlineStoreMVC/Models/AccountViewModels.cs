﻿using System.ComponentModel.DataAnnotations;

namespace OnlineStoreMVC.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required(ErrorMessage = "{0} اجباری است")]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور فعلی")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [StringLength(100, ErrorMessage = "{0} باید حد اقل {2} کاراکتر باشد", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور جدید")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار کلمه عبور جدید")]
        [Compare("NewPassword", ErrorMessage = "کلمه عبور و تکرار آن برابر نیست")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [Display(Name = "مرا به خاطر بسپار")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "{0} اجباری است")]
        [System.Web.Mvc.Remote("IsUserExists", "Account", ErrorMessage = "نام کاربری تکراری است")] 
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [StringLength(50, ErrorMessage = "{0} باید حد اقل {2} کاراکتر باشد", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار کلمه عبور")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "کلمه عبور و تکرار آن برابر نیست")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "ایمیل")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "{0} به درستی وارد نشده است")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "آدرس")]
        public string Address { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "{0} درست وارد نشده است")]
        [Display(Name = "تلفن")]
        public string Telephone { get; set; }
    }

    public class EditViewModel
    {
        public EditViewModel() { }

        public EditViewModel(User user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Address = user.Address;
            Telephone = user.Telephone;
        }
        [Key]
        public long Id { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "ایمیل")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "{0} به درستی وارد نشده است")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "آدرس")]
        public string Address { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "{0} درست وارد نشده است")]
        [Display(Name = "تلفن")]
        public string Telephone { get; set; }
    }
}
