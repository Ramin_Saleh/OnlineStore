﻿using System.ComponentModel.DataAnnotations;

namespace OnlineStoreMVC.Models
{
    public class InvoiceModel
    {
        public InvoiceModel() { }

        public InvoiceModel(vw_Invoice ivc)
        {
            this.PaidPrice = ivc.PaidPrice ?? 0;
            this.UserName = ivc.FirstName + " " + ivc.LastName;
            this.GoodCode = ivc.Code;
            this.GoodName = ivc.Name;
            this.Price = ivc.Price ?? 0;
            this.Id = ivc.Id;
        }
        [Key]
        public long Id;

        [Display(Name = "مشتری")]
        public string UserName { get; set; }
        [Display(Name = "کد کالا")]
        public string GoodCode { get; set; }
        [Display(Name = "نام کالا")]
        public string GoodName { get; set; }
        [Display(Name = "قیمت")]
        public long Price { get; set; }
        [Display(Name = "قیمت پرداخت شده")]
        public long PaidPrice { get; set; }
    }
}