﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using OnlineStoreMVC.Models;
using System.Web.Security;
using System.Security.Cryptography;

namespace OnlineStoreMVC.Controllers
{
    [Authorize]
    public class AccountController : ApplicationBaseController
    {
        private bool UserExists(string username, string password)
        {
            using (OnlineStoreEntities contex = new OnlineStoreEntities())
            {
                SHA1 sha = new SHA1CryptoServiceProvider();
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(password);
                byte[] hashpass = sha.ComputeHash(bytes);
                return contex.Users.Any(x => x.Username == username && x.Password == hashpass);
            }
        }
        private ActionReport AddUser(RegisterViewModel model)
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    if (context.Users.Any(x => x.Username == model.UserName))
                    {
                        report.HasError = true;
                        report.Message = "نام کاربری تکراری است";
                        return report;
                    }
                    User user = new User();
                    user.Username = model.UserName;
                    SHA1 sha = new SHA1CryptoServiceProvider();
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(model.Password);
                    byte[] hashpass = sha.ComputeHash(bytes);
                    user.Password = hashpass;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Telephone = model.Telephone;
                    user.Address = model.Address;
                    user.Email = model.Email;
                    context.Users.Add(user);
                    context.SaveChanges();
                    return report;
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }
        private ActionReport EditUser(EditViewModel model, long id)
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    var user = context.Users.Find(id);
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Telephone = model.Telephone;
                    user.Address = model.Address;
                    user.Email = model.Email;
                    context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    return report;
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }
        [HttpGet]
        [Authorize]
        public ActionResult EditPassword()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public ActionResult EditPassword(ManageUserViewModel model)
        {
            var user = GetUserByUsername(User.Identity.Name);
            if (UserExists(user.Username, model.OldPassword))
            {
                var report = ChangePassword(user.Id, model.NewPassword);
                if (!report.HasError)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", report.Message);
            }
            else
            {
                ModelState.AddModelError("", "کلمه عبور اشتباه است");
            }
            return View(model);
        }

        private ActionReport ChangePassword(long id, string password)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(password);
            byte[] hashpass = sha.ComputeHash(bytes);
            ActionReport report = new ActionReport();
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                try
                {
                    var user = context.Users.Find(id);
                    user.Password = hashpass;
                    context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
            return report;
        }
        private User GetUserByID(long id)
        {
            using (OnlineStoreEntities contex = new OnlineStoreEntities())
            {
                return contex.Users.Find(id);
            }
        }
        private User GetUserByUsername(string username)
        {
            using (OnlineStoreEntities contex = new OnlineStoreEntities())
            {
                return contex.Users.SingleOrDefault(x=> x.Username == username);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult IsUserExists(string Username)
        {
            using (OnlineStoreEntities db = new OnlineStoreEntities())
                return Json(!db.Users.Any(x => x.Username == Username), JsonRequestBehavior.AllowGet);
        } 
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return RedirectToLocal(returnUrl);
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return RedirectToLocal(returnUrl);
            if (ModelState.IsValid)
            {
                if (UserExists(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "نام کاربری یا کلمه عبور اشتباه است");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var report = AddUser(model);
                if (!report.HasError)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", report.Message);
            }
            else
                ModelState.AddModelError("", "عملیات با مشکل مواجه شد");
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Edit()
        {
            var user = GetUserByUsername(User.Identity.Name);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(new EditViewModel(user));
        }

        //
        // POST: /Movies/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(EditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = GetUserByUsername(User.Identity.Name);
                if (user == null)
                {
                    return HttpNotFound();
                }
                var report = EditUser(model, user.Id);
                if(!report.HasError)
                    return RedirectToAction("Index", "Home");
                else
                    ModelState.AddModelError("", report.Message);
            }
            return View(model);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        [Authorize(Roles="admin")]
        public ActionResult AdminPanel()
        {
            return View();
        }
        ////
        //// POST: /Account/Disassociate
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        //{
        //    ManageMessageId? message = null;
        //    IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
        //    if (result.Succeeded)
        //    {
        //        message = ManageMessageId.RemoveLoginSuccess;
        //    }
        //    else
        //    {
        //        message = ManageMessageId.Error;
        //    }
        //    return RedirectToAction("Manage", new { Message = message });
        //}

        ////
        //// GET: /Account/Manage
        //public ActionResult Manage(ManageMessageId? message)
        //{
        //    ViewBag.StatusMessage =
        //        message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
        //        : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
        //        : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
        //        : message == ManageMessageId.Error ? "An error has occurred."
        //        : "";
        //    ViewBag.HasLocalPassword = HasPassword();
        //    ViewBag.ReturnUrl = Url.Action("Manage");
        //    return View();
        //}

        ////
        //// POST: /Account/Manage
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Manage(ManageUserViewModel model)
        //{
        //    bool hasPassword = HasPassword();
        //    ViewBag.HasLocalPassword = hasPassword;
        //    ViewBag.ReturnUrl = Url.Action("Manage");
        //    if (hasPassword)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
        //            if (result.Succeeded)
        //            {
        //                return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
        //            }
        //            else
        //            {
        //                AddErrors(result);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        // User does not have a password so remove any validation errors caused by a missing OldPassword field
        //        ModelState state = ModelState["OldPassword"];
        //        if (state != null)
        //        {
        //            state.Errors.Clear();
        //        }

        //        if (ModelState.IsValid)
        //        {
        //            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
        //            if (result.Succeeded)
        //            {
        //                return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
        //            }
        //            else
        //            {
        //                AddErrors(result);
        //            }
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        ////
        //// POST: /Account/ExternalLogin
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    // Request a redirect to the external login provider
        //    return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        //}

        ////
        //// GET: /Account/ExternalLoginCallback
        //[AllowAnonymous]
        //public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Login");
        //    }

        //    // Sign in the user with this external login provider if the user already has a login
        //    var user = await UserManager.FindAsync(loginInfo.Login);
        //    if (user != null)
        //    {
        //        await SignInAsync(user, isPersistent: false);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    else
        //    {
        //        // If the user does not have an account, then prompt the user to create an account
        //        ViewBag.ReturnUrl = returnUrl;
        //        ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
        //        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
        //    }
        //}

        ////
        //// POST: /Account/LinkLogin
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LinkLogin(string provider)
        //{
        //    // Request a redirect to the external login provider to link a login for the current user
        //    return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        //}

        ////
        //// GET: /Account/LinkLoginCallback
        //public async Task<ActionResult> LinkLoginCallback()
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        //    }
        //    var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
        //    if (result.Succeeded)
        //    {
        //        return RedirectToAction("Manage");
        //    }
        //    return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        //}

        ////
        //// POST: /Account/ExternalLoginConfirmation
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        return RedirectToAction("Manage");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        // Get the information about the user from the external login provider
        //        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
        //        if (info == null)
        //        {
        //            return View("ExternalLoginFailure");
        //        }
        //        var user = new ApplicationUser() { UserName = model.UserName };
        //        var result = await UserManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //            if (result.Succeeded)
        //            {
        //                await SignInAsync(user, isPersistent: false);
        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        ////
        //// POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LogOff()
        //{
        //    AuthenticationManager.SignOut();
        //    return RedirectToAction("Index", "Home");
        //}

        ////
        //// GET: /Account/ExternalLoginFailure
        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}

        //[ChildActionOnly]
        //public ActionResult RemoveAccountList()
        //{
        //    var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
        //    ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
        //    return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && UserManager != null)
        //    {
        //        UserManager.Dispose();
        //        UserManager = null;
        //    }
        //    base.Dispose(disposing);
        //}

        #region Helpers
        //// Used for XSRF protection when adding external logins
        //private const string XsrfKey = "XsrfId";

        //private IAuthenticationManager AuthenticationManager
        //{
        //    get
        //    {
        //        return HttpContext.GetOwinContext().Authentication;
        //    }
        //}

        //private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        //{
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //    var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        //    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        //}

        //private void AddErrors(IdentityResult result)
        //{
        //    foreach (var error in result.Errors)
        //    {
        //        ModelState.AddModelError("", error);
        //    }
        //}

        //private bool HasPassword()
        //{
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        return user.PasswordHash != null;
        //    }
        //    return false;
        //}

        //public enum ManageMessageId
        //{
        //    ChangePasswordSuccess,
        //    SetPasswordSuccess,
        //    RemoveLoginSuccess,
        //    Error
        //}

        //private class ChallengeResult : HttpUnauthorizedResult
        //{
        //    public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
        //    {
        //    }

        //    public ChallengeResult(string provider, string redirectUri, string userId)
        //    {
        //        LoginProvider = provider;
        //        RedirectUri = redirectUri;
        //        UserId = userId;
        //    }

        //    public string LoginProvider { get; set; }
        //    public string RedirectUri { get; set; }
        //    public string UserId { get; set; }

        //    public override void ExecuteResult(ControllerContext context)
        //    {
        //        var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
        //        if (UserId != null)
        //        {
        //            properties.Dictionary[XsrfKey] = UserId;
        //        }
        //        context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        //    }
        //}
        #endregion
    }
}