﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineStoreMVC.Models;

namespace OnlineStoreMVC.Controllers
{
    public class GoodController : ApplicationBaseController
    {
        private ActionReport AddGood(GoodRegisterViewModels model)
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    if (context.Goods.Any(x => x.Name == model.Name))
                    {
                        report.HasError = true;
                        report.Message = "نام کالا تکراری است";
                        return report;
                    }
                    Good good = new Good();
                    good.Name = model.Name;
                    good.IdGroup = model.IdGroup;
                    good.Description = model.Description;
                    good.Author = model.Author;
                    good.Translator = model.Translator;
                    good.Price = model.Price;
                    good.Publisher = model.Publisher;
                    good.Edition = model.Edition;
                    good.Code = model.Code;
                    context.Goods.Add(good);
                    context.SaveChanges();
                    return report;
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }

        private ActionReport EditGood(GoodEditViewModels model)
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    Good good = context.Goods.Find(model.Id);
                    good.Name = model.Name;
                    good.Description = model.Description;
                    good.Author = model.Author;
                    good.Code = model.Code;
                    good.Price = model.Price;
                    good.Publisher = model.Publisher;
                    good.Translator = model.Translator;
                    good.IdGroup = model.IdGroup;
                    context.Entry(good).State = EntityState.Modified;
                    context.SaveChanges();
                    return report;
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }
        private List<Good> GetGoodList(long? idgrp, string searchstring)
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                bool checkfilter = string.IsNullOrWhiteSpace(searchstring);
                return context.Goods.Where(x => (!idgrp.HasValue || x.IdGroup == idgrp) && (checkfilter || x.Name.Contains(searchstring))).ToList();
            }
        }

        private Good GetGoodById(long id)
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                return context.Goods.Find(id);
            }
        }
        private ActionReport DeleteGoodById(long id)
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    Good good = context.Goods.Find(id);
                    context.Goods.Remove(good);
                    context.SaveChanges();
                    return report;
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }

        private List<Group> GetGroupsList()
        {
            using (OnlineStoreEntities context = new OnlineStoreEntities())
            {
                return context.Groups.ToList();
            }
        }

        // GET: /Good/
        public ActionResult Index(long? idgrp, string searchstring)
        {
            return View(GetGoodList(idgrp, searchstring).Select(x => new GoodViewModel(x)));
        }

        // GET: /Good/
        public ActionResult _PartialIndex(long? idgrp)
        {
            return PartialView(GetGoodList(idgrp, null).Select(x => new GoodViewModel(x)));
        }

        // GET: /Good/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Good good = GetGoodById(id.Value);
            if (good == null)
            {
                return HttpNotFound();
            }
            return View(new GoodViewModel(good));
        }

        // GET: /Good/Create
        [HttpGet]
        [Authorize(Roles="admin")]
        public ActionResult Create()
        {
            ViewBag.Groups = new SelectList(GetGroupsList().Where(x=> x.Level == 3), "Id", "Description");
            return View();
        }

        // POST: /Good/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult Create(GoodRegisterViewModels model)
        {
            if (ModelState.IsValid)
            {
                var report = AddGood(model);
                if (!report.HasError)
                    return RedirectToAction("Index");
                else
                    ModelState.AddModelError("", report.Message);
            }
            return View(model);
        }

        // GET: /Good/Edit/5
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Good good = GetGoodById(id.Value);
            if (good == null)
            {
                return HttpNotFound();
            }
            ViewBag.Groups = new SelectList(GetGroupsList().Where(x => x.Level == 3), "Id", "Description", good.IdGroup);
            return View(new GoodEditViewModels(good));
        }

        // POST: /Good/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(GoodEditViewModels model)
        {
            if (ModelState.IsValid)
            {
                var report = EditGood(model);
                if (!report.HasError)
                    return RedirectToAction("Index");
                else
                    ModelState.AddModelError("", report.Message);
            }
            return View(model);
        }

        [Authorize(Roles = "admin")]
        // GET: /Good/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Good good = GetGoodById(id.Value);
            if (good == null)
            {
                return HttpNotFound();
            }
            return View(new GoodViewModel(good));
        }

        // POST: /Good/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteConfirmed(long id)
        {
            var report = DeleteGoodById(id);
            return RedirectToAction("Index");
        }
    }
}
