﻿using OnlineStoreMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineStoreMVC.Controllers
{
    public class InvoiceController : ApplicationBaseController
    {
        private User GetUserByUsername(string username)
        {
            using (OnlineStoreEntities contex = new OnlineStoreEntities())
            {
                return contex.Users.SingleOrDefault(x => x.Username == username);
            }
        }
        private ActionReport AddGoodToInvoice(long idgood, long iduser)
        {
            using(var context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    Invoice invoice = new Invoice();
                    invoice.IdGood = idgood;
                    invoice.IdUser = iduser;
                    invoice.IdInvoiceState = 1;
                    context.Invoices.Add(invoice);
                    context.SaveChanges();
                    return report;
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }
        private List<vw_Invoice> GetCurrentUserInvoices()
        {
            using(var context = new OnlineStoreEntities())
            {
                var user = context.Users.SingleOrDefault(x=> x.Username == User.Identity.Name);
                return context.vw_Invoice.Where(x => x.IdUser == user.Id).ToList();
            }
        }
        private List<vw_Invoice> GetAllUsersPaidInvoices()
        {
            using(var context = new OnlineStoreEntities())
            {
                return context.vw_Invoice.Where(x => x.IdInvoiceState == 2).ToList();
            }
        }
        private int GetCurrentUserCartCount()
        {
            using (var context = new OnlineStoreEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Username == User.Identity.Name);
                return context.vw_Invoice.Count(x => x.IdUser == user.Id && x.IdInvoiceState == 1);
            }
        }


        private ActionReport PurchaseGoods()
        {
            using (var context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    var user = context.Users.SingleOrDefault(x => x.Username == User.Identity.Name);
                    var ivcs = context.Invoices.Where(x => x.IdUser == user.Id && x.IdInvoiceState == 1);
                    foreach (var ivc in ivcs)
                    {
                        ivc.IdInvoiceState = 2;
                        var good = context.Goods.Find(ivc.IdGood);
                        if (good != null)
                            ivc.PaidPrice = good.Price;
                        context.Entry(ivc).State = System.Data.Entity.EntityState.Modified;
                    }
                    context.SaveChanges(); 
                    return report;
                }
                catch (Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }
        private ActionReport DeleteInvoiceById(long id, long userid)
        {
            using(var context = new OnlineStoreEntities())
            {
                ActionReport report = new ActionReport();
                try
                {
                    var invoice = context.Invoices.SingleOrDefault(x => x.IdUser == userid && x.Id == id);
                    if (invoice != null)
                    {
                        context.Invoices.Remove(invoice);
                        context.SaveChanges();
                    }
                    else
                    {
                        report.HasError = true;
                        report.Message = "شما به این فاکتور دسترسی ندارید";
                        return report;
                    }
                    return report;
                }
                catch(Exception ex)
                {
                    report.HasError = true;
                    report.Message = ex.Message;
                    return report;
                }
            }
        }
        //
        // GET: /Invoice/
        [Authorize]
        public ActionResult _PartialInvoiceList()
        {
            var items = GetCurrentUserInvoices().Where(x=> x.IdInvoiceState == 1).Select(x => new InvoiceModel(x)).ToList();
            return PartialView(items);
        }
        //
        // GET: /Invoice/
        [HttpGet]
        [Authorize(Roles="admin")]
        public ActionResult _LastPaidInvoices()
        {
            var items = GetAllUsersPaidInvoices().Select(x => new InvoiceModel(x)).ToList();
            return PartialView(items);
        }
        //
        // GET: /Invoice/
        [Authorize]
        public ActionResult InvoiceList()
        {
            var items = GetCurrentUserInvoices().Where(x => x.IdInvoiceState == 1).Select(x => new InvoiceModel(x)).ToList();
            return View(items);
        }

        //
        // POST: /Invoice/
        [HttpPost]
        [Authorize]
        public ActionResult InvoiceList(IEnumerable<InvoiceModel> model)
        {
            var report = PurchaseGoods();
            if (!report.HasError)
                return RedirectToAction("Index", "Home", null);
            else
                ModelState.AddModelError("", report.Message);
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public JsonResult AddToCart(long idgood)
        {
            var user = GetUserByUsername(User.Identity.Name);
            var report = AddGoodToInvoice(idgood, user.Id);
            return Json(report.Message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Delete(long id)
        {
            var user = GetUserByUsername(User.Identity.Name);
            var report = DeleteInvoiceById(id, user.Id);
            if (!report.HasError)
                return Json("Deleted", JsonRequestBehavior.AllowGet);
            else
                return Json(report.Message, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult GetCartCount()
        {
            return Json(GetCurrentUserCartCount(), JsonRequestBehavior.AllowGet);
        }
	}
}