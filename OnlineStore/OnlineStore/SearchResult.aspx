﻿<%@ Page Title="نتایج جستجو" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchResult.aspx.cs" Inherits="OnlineStore.SearchResult" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .sidebarfilter td{
            padding: 5px;
        }
        .results td{
            width: 25%;
            padding: 10px;
        }
         .results span{
            display: block;
            text-align: center;
        }
        .results div{
            border: 1px #888 solid;
            border-radius: 5px;
        }
        .results img{
            margin-top: 10px;
        }
        a.linkAdd{
            color:aqua;
        }
        a.linkAdd:hover{
            color:blue;
        }
    </style>
    <div style="width:100%; background: #000; font-family: 'B Yekan'; font-size: 13px;">
        <table style="width:100%; direction:rtl;">
            <tr>
                <td style="width:200px;">
                    <div style="border-radius: 4px; border: 1px #888 solid; color:#888;">
                        <div style="background:#444;">
                            فیلترها
                        </div>
                        <div>
                            <table class="sidebarfilter">
                                <tr>
                                    <td colspan="2">
                                        <input type="text" placeholder="رنگ" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" placeholder="از قیمت" style="width:80px;"/>
                                    </td>
                                    <td>
                                        <input type="text" placeholder="تا قیمت" style="width:80px;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="text" placeholder="برند" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
                <td>
                    <div style="border-radius: 4px; border: 1px #888 solid; color:#888; width:100%; vertical-align:top; height: 500px;">
                        <div style="margin-right: 10px;">
                            جستجوی کالا (3) نتیجه
                        </div>
                        <%--<hr style="color: #888; width:80%; margin-left:auto; margin-right:auto;"/>--%>
                        <div>
                            <table style="margin-right:10px; width:100%;">
                                <tr>
                                    <td>
                                        <input type="text" id="txtSearch" placeholder="جستجو ..." onfocus="$(this).css('color', '#3c3c3c');" style="color: rgb(60, 60, 60); width:100%;" />
                                    </td>
                                    <td>
                                        <a href="#" class="dk-button green" style="margin:10px; width:100px;">
						                    <div class="dk-button-container">
							                    جستجو
						                    </div>
					                    </a>
                                    </td>
                                </tr>
                            </table>
                            <%--<hr style="color: #888; width:80%; margin-left:auto; margin-right:auto;"/>--%>
                            <table class="results" style="text-align:center;">
                                <tr>
                                    <td>
                                        <div>
                                            <a href="#">
                                                <img src="Images/z5premium.jpg" />
                                            </a>
                                            <span>Xperia Z5 Premium</span>
                                            <span>قیمت: 2,700,000 تومان</span>
                                            <a class="linkAdd" href="#" onclick="addtolist(1);">
                                                اضافه به لیست خرید
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <a href="GoodSpc.aspx">
                                                <img src="Images/z5.jpg" />
                                            </a>
                                            <span>Xperia Z5</span>
                                            <span>قیمت: 2,300,000 تومان</span>
                                            <a class="linkAdd" href="#" onclick="addtolist(2);">
                                                اضافه به لیست خرید
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <a href="#">
                                                <img src="Images/z5compact.jpg" />
                                            </a>
                                            <span>Xperia Z5 Compact</span>
                                            <span>قیمت: 1,800,000 تومان</span>
                                            <a class="linkAdd" href="#" onclick="addtolist(3);">
                                                اضافه به لیست خرید
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
