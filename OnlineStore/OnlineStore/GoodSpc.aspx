﻿<%@ Page Title="مشخصات کالا" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GoodSpc.aspx.cs" Inherits="OnlineStore.GoodSpc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div style="direction: rtl; background: #484848; border-radius:4px; box-shadow: 1px; height:600px;">
        <table style="width: 100%;">
            <tr>
                <td>
                    <img src ="Images/XperiaZ5.png" style="padding:10px;"/>
                </td>
                <td>
                    <header style="margin: 20px; direction:ltr;">
                        <h2 style="color: yellow;">
                            Sony Xperia Z5
                        </h2>
                    </header>
                    <div style="height: 500px; direction:rtl; color:#aaa; font-family:'B Yekan'; font-size:large;">
                        <div>
                            <div>
                                <span style="color: yellow;">
                                    قیمت: 
                                </span>
                                <span>
                                    2,300,000
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    وضعیت:
                                </span>
                                <span style="display: inline;">
                                    موجود
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    مشخصات کلی:
                                </span>
                                <span style="display: inline;">
                                    یک سیم کارته - قطع سیم کارت: سایز نانو (8.8 × 12.3 میلی‌متر) - وزن : 154 گرم - مجهز به حس‌گر اثرانگشت ، مقاوم در برابر آب ، مناسب عکاسی
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    پردازنده:
                                </span>
                                <span style="display: inline;">
                                    Qualcomm Snapdragon 810 Chipset - از نوع 64 بیت
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    صفحه نمایش:
                                </span>
                                <span style="display: inline;">
                                   صفحه نمایش رنگی لمسی با سایز 5.2 اینچو رزولوشن 1920 × 1080 | FullHD دارای تراکم 428 پیکسل بر هر اینچ - قابلیت نمایش 16 میلیون رنگ
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    حافظه:
                                </span>
                                <span style="display: inline;">
                                   حافظه‌ داخلی 32 گیگابایت - مقدار رم 3 گیگابایت - پشتیبانی از کارت حافظه‌ی microSD تا حداکثر ظرفیت 200 گیگابایت
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    ارتباطات:
                                </span>
                                <span style="display: inline;">
                                   پشتیبانی از شبکه 2G ، 3G ، 4G - مجهز به GPRS ، EDGE ، Wi-Fi ، بلوتوث ، NFC ، رادیو ، OTG
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    دوربین:
                                </span>
                                <span style="display: inline;">
                                   23 مگاپیکسل - فوکوس اتوماتیک - فلش LED
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    صدا:
                                </span>
                                <span style="display: inline;">
                                    اسپیکر استریو - جک 3.5 میلیمتری صدا
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    امکانات نرم افزاری:
                                </span>
                                <span style="display: inline;">
                                    سیستم عامل Android Lollipop 5.1 - پشتیبانی از زبان فارسی - امکاناتی نظیر MMS ، ایمیل ، مرورگر HTML5 ، قابلیت نمایش اسناد مایکروسافت آفیس ، قابلیت نمایش فایل‌های متنی PDF ، قابلیت استفاده از سرویس شبکه‌های اجتماعی ، برنامه ویرایش عکس ، سرویس‌های گوگل شامل Google Search, Google Maps, Gmail و YouTube
                                </span>
                            </div>
                            <div>
                                <span style="color: yellow;">
                                    سایر مشخصات:
                                </span>
                                <span style="display: inline;">
                                    باتری از نوع لیتیوم-یون با ظرفیت 2930 میلی‌آمپر ساعت قابلیت شارژ سریع نسخه 2 (60 درصد شارژ در 30 دقیقه) (Quick Charge 2.0) - اقلام همراه گوشی : دفترچه‌ راهنما ، کابل USB ، شارژر ، هندزفری
                                </span>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
