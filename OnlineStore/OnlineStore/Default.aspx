﻿<%@ Page Title="صفحه اصلی" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OnlineStore._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <script type="text/javascript">
            var currentbanner = 0;
            function showbanner(id) {
                $('.clickCount[bannerid="' + currentbanner + '"] img').hide();
                $('.clickCount[bannerid="' + id + '"] img').show();
                currentbanner = id;
            }
        </script>
        <div id="content">
            <article id="slider" class="dkbox mrg-bottom home">
				<div class="slides center">
					<a class="clickCount" elementtype="1" categorytitle="اوج هنر و خلاقیت در گوشی های هوآوی" bannerid="1" href="#">
						<img style="display: block;" src="Images/huawei.jpg" alt="اوج هنر و خلاقیت در گوشی های هوآوی" title="اوج هنر و خلاقیت در گوشی های هوآوی" class="slideItem" height="310" width="890">
					</a>
					<a class="clickCount" elementtype="1" categorytitle="فروش ویژه پاوربانک" bannerid="2" href="#">
					    <img style="display: none;" src="Images/powerbank.jpg" alt="فروش ویژه پاوربانک" title="فروش ویژه پاوربانک" class="slideItem" height="310" width="890">
					</a>
					<a class="clickCount" elementtype="1" categorytitle="برترین گوشی های 4g" bannerid="3" href="#">
					    <img style="display: none;" src="Images/4g.jpg" alt="برترین گوشی های 4g" title="برترین گوشی های 4g" class="slideItem" height="310" width="890">
					</a>
					<a class="clickCount" elementtype="1" categorytitle="طراحی بی نظیر ولی تکراری" bannerid="4" href="#">
					    <img style="display: none;" src="Images/sony.jpg" alt="طراحی بی نظیر ولی تکراری" title="طراحی بی نظیر ولی تکراری" class="slideItem" height="310" width="890">
					</a>
					<a class="clickCount" elementtype="1" categorytitle="نقد و بررسی موبایل" bannerid="5" href="#">
					    <img style="display: none;" src="Images/naghd.jpg" alt="نقد و بررسی موبایل" title="نقد و بررسی موبایل" class="slideItem" height="310" width="890">
					</a>
					<footer>
						<ul class="tabs">
							<li class="tabItem">
								<a href="javascript:void(0)" title="اوج هنر و خلاقیت در گوشی های هوآوی" onmouseover="showbanner(1);">گوشی های هوآوی</a>
							</li>
							<li class="sep">
								<span></span>
							</li>
							<li class="tabItem">
								<a href="javascript:void(0)" title="" onmouseover="showbanner(2);">فروش ویژه پاوربانک</a>
							</li>
							<li class="sep">
								<span></span>
							</li>
							<li class="tabItem">
								<a href="javascript:void(0)" title="" onmouseover="showbanner(3);">برترین گوشی های 4g
								</a>
							</li>
                            <li class="sep">
								<span></span>
							</li>
							<li class="tabItem">
								<a href="javascript:void(0)" title="طراحی بی نظیر ولی تکراری" onmouseover="showbanner(4);">گوشی sony xperia z5
								</a>
							</li>
							<li class="sep">
							<span></span>
							</li>
							<li class="tabItem">
								<a href="javascript:void(0)" title="نقد و بررسی موبایل" onmouseover="showbanner(5);">نقد و بررسی موبایل</a>
							</li>
						</ul>
					</footer>
				</div>
				<a style="display: none;" href="#" class="backward hidden">
				</a>
				<a style="display: none;" href="#" class="forward hidden">
				</a>
			</article>
		</div>

		<div id="sidebar">
			<aside>
				<article id="tv" class="dkbox mrg-bottom">
					<header>
        				<h3>جدیدترین ویدیوها</h3>
    				</header>
   					 <div class="tvbox">
        				<div id="videoItems" class="items">
        					<div class="item">
								<a href="#" title="نقد و بررسي سوني اکسپريا Z5 کامپکت" class="ii">
        							<img src="" alt="نقد و بررسي سوني اکسپريا Z5 کامپکت" height="62" width="110">
        							<span class="mt rtl">10:02
        							</span>
        							<div class="over">
        							</div>
        						</a>
        						<div class="dc rtl">
        							<a class="at rtl" href="#" title="نقد و بررسي سوني اکسپريا Z5 کامپکت">
        								<h3>نقد و بررسي سوني اکسپريا Z5 کامپکت</h3>
        						</a>
        						<span>بازدید : 2294
        						</span>
        						</div>
        					</div>
        					<div class="sep"></div>
        					<div class="item">
        						<a href="#" title="مقايسه هواوي P8 و P8 لايت" class="ii">
        							<img src="" alt="مقايسه هواوي P8 و P8 لايت" height="62" width="110">
        							<span class="mt rtl">8:07</span>
        							<div class="over"></div>
        						</a>
        						<div class="dc rtl">
        							<a class="at rtl" href="#" title="مقايسه هواوي P8 و P8 لايت">
        								<h3>مقايسه هواوي P8 و P8 لايت</h3>
        							</a>
        							<span>بازدید : 10952</span>
        						</div>
        					</div>
        					<div class="sep"></div>
        					<div class="item">
        						<a href="#" title="نقد و بررسي ال جي G4" class="ii">
        							<img src="" alt="نقد و بررسي ال جي G4" height="62" width="110">
        							<span class="mt rtl">14:53</span>
        							<div class="over"></div>
        						</a>
        						<div class="dc rtl">
        							<a class="at rtl" href="#" title="نقد و بررسي ال جي G4">
        								<h3>نقد و بررسي ال جي G4</h3>
        							</a>
        							<span>بازدید : 16259</span>
        						</div>
        					</div>
        					<div class="more rtl">
        						<a href="#">مشاهده ویدیوهای بیشتر</a>
        					</div>
        				</div>
    				</div>
                </article>
			</aside>
		</div>
</asp:Content>
